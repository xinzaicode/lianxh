!* Verion: 2.0 
!* Update: 2020/11/9 20:50
* https://www.lianxh.cn
* Author: 连玉君 (Email: arlionn@163.com)
*         康峻杰 (Email: 642070192@qq.com)
*         刘庆庆 (Email: 2428172451@qq.com)

* 在线文本比对，可以查看各个版本的变化 2020/11/6 22:54
*    http://wenbenbijiao.renrensousuo.com/

/*
版本记录：
- 2020/11/10 11:02 
  添加【隐藏功能】 lianxh sj, lianxh book
*/

cap program drop lianxh
program define lianxh

version 15.0
	
syntax [anything(name = class)] [, ///
        Mlink           ///
	    MText           ///
	    NOTime          /// 
	    Saving(string)  ///
	   ]
	
*==============================================================================*
**** 数据处理 ****

preserve

clear    // 避免变量与用户变量冲突

	tempfile lxh_temp lxh_title catID catSort Final_data outcome mycovs1 combine
	// 利用preserve和tempfile，避免对用户数据及文件产生影响
	
	*------------------------------------------------------------------------*
	***对输入options进行预设***
	
	if "`notime'" == "" {
		local notime "0"
	}
	else {
		local notime "1"	
	}
	
	if `"`saving'"'~="" {
		local saving = subinstr(`"`saving'"',`"""',"",.)
		local saving = subinstr(`"`saving'"',"\","/",.)
		
		local path2 = ustrreverse("`saving'")
		local index_fullname = index(`"`path2'"',"/")
		if `index_fullname' ~= 0{
			local path = ustrreverse(substr(`"`path2'"',`=`index_fullname'+1',.))
			local saving = ustrreverse(substr(`"`path2'"',1,`=`index_fullname'-1'))		
		}
		local beg_dot = index(`"`saving'"',".")
		if `beg_dot'~=0 {
			local suffixname = substr(`"`saving'"',`=`beg_dot'+1',.)
			if ~inlist("`suffixname'","txt","csv","md"){
			   noi dis in red "Only [.txt .csv .md] files are supported by {opt saving()}"
			   exit 
			}
			local strippedname = substr(`"`saving'"',1,`=`beg_dot'-1')
		}
		else {
			local strippedname `"`saving'"'
			local suffixname = "md"
		}
		local saving `"`strippedname'.`suffixname'"'
	}
	
	if "`path'" ~= "" {
		capture mkdir `path'
	} 
	else {
		local path `c(pwd)'
	}
	
	if "`mlink'" ~= "" {
		local mlink "1"
	}
	else {
		local mlink "0"
	}
	
	if "`mtext'" ~= "" {
		local mtext "1"
	}
	else {
		local mtext "0"	
	}
	
	*------------------------------------------------------------------------*
	***正则表达式抽取网址 标题 分类标签等***
	
	if ustrregexm(`"`path'"', "(/|\\)$") {
		local path = ustrregexrf(`"`path'"', ".$", "")
	}
	
	local address "https://www.lianxh.cn/blogs.html" 

	tempfile address_text  
	
	capture copy `"`address'"' "`address_text'.txt", replace   
	local times = 0
	while _rc ~= 0 {
		local times = `times' + 1
		sleep 1000
		cap copy `"`address'"' "`address_text'.txt", replace
		if `times' > 10 {
			disp as error "Internet speeds is too low to get the data"
			exit 601
		}
	}
	
	capture{
		cap infix strL v 1-1000 using "`address_text'.txt", clear
		cap save "`lxh_temp'", replace
		
		use "`lxh_temp'", replace
		*- 抽取原始链接
		local regex    = `"(?<="><a href=")(.*)(?=")"' 
		* 以 「"><a href="」开头，「"」结尾的字符串 
		gen link       = ustrregexs(0) if ustrregexm(v, `"`regex'"')
		replace link   = "https://www.lianxh.cn" + link if link!=""
		
		*- 抽取标题
		local regex    = `"(?<=html">)(.+)(?=</a></h3>)"'
		gen title      = ustrregexs(0) if ustrregexm(v, `"`regex'"')
		
		*- 抽取分类标签
		local regex    = `"(<span>)(\D.*)(?:</span>)"'  
		*-Note: 需要|<span>结果输出</span>|，而不需要 |<span>04/02</span>|, 所以，使用「\D」排除后者
		gen cat        = ustrregexs(2) if ustrregexm(v, `"`regex'"')	
		drop if ustrregexm(cat, "(data\[item\])")
		*br if link!   =""|cat!=""
		keep if link!  =""|cat!=""
		replace cat    = cat[_n+1] if cat==""
		keep if title! =""
		drop v
		drop if ustrregexm(title, "(data\[item\])")
		format title %-60s
		gen id         = _n         // 推文发表时间序号
		save "`lxh_title'", replace 	
		
		*- 产生分类文件
		use "`lxh_temp'", replace
		local regex    = `"(?<=/blogs/)\d+(?=\.html)"'
		gen catID      = ustrregexs(0) if ustrregexm(v, `"`regex'"')  // 分类编号

		keep if catID != ""
		local regex    = `"(?<=\s>).+(?=</a>)"'
		gen cat        = ustrregexs(0) if ustrregexm(v, `"`regex'"')  // 分类名称
		drop v 
		destring catID, replace 
		cap save "`catID'", replace  // 临时保存文件，随后与主文件合并 

		*- 利用矩阵形式
		#d ;
		matrix test = ( 44	,	1	\
						34	,	2	\
						31	,	3	\
						43	,	10	\
						16	,	8	\
						17	,	9	\
						18	,	4	\
						35	,	11	\
						25	,	12	\
						24	,	13	\
						26	,	14	\
						22	,	17	\
						32	,	21	\
						20	,	22	\
						38	,	24	\
						39	,	26	\
						40	,	28	\
						41	,	30	\
						42	,	32	\
						19	,	43	\
						21	,	45	\
						28	,	47	\
						29	,	49	\
						27	,	51	\
						36	,	61	\
						37	,	63	\
						45	,	96	\
						30	,	97	\
						23	,	98	\
						33	,	99	) ;
		#d cr 
		xsvmat test, saving("`mycovs1'", replace) 
		use "`mycovs1'",clear
		rename test1 catID
		rename test2 cat123
		save "`combine'",replace
		
		use "`catID'", replace
		merge 1:1 catID using "`combine'", nogen 
		compress 
		tostring catID, gen(catID_str)
		*- https://www.lianxh.cn/blogs/20.html
		local url "https://www.lianxh.cn/blogs/"
		gen linkCat   = "[" + cat + "](" + "`url'" + catID_str + ".html" + ")"
		cap save "`catSort'", replace
		
		
		*- 合并文件
		use "`catID'", replace 
		merge 1:1 catID using "`catSort'", nogen 
		merge 1:m cat   using "`lxh_title'", nogen 
		gen list      =  "- [" + title + "](" + link + ")"
		gen list2     =  " [" + title + "](" + link + ")"
		*gen website  = `"{browse ""' + link +`"": ["' + title +`"]}"'	
		gen website   = `"{browse ""' + link +`"": "' + title +`"}"'
		gen website2  = `"{browse ""' + "`url'" + catID_str +`"": "' + cat +`"}"'
		*gen list2    = "  - [" + title + "](" + link + ")"
		
		save "`Final_data'", replace
		use  "`Final_data'", clear
		*- 前期数据处理完毕	
	}

	
*==============================================================================*
**** 输入变量识别 ****

	*------------------------------------------------------------------------*
	***class识别***
	
	*-隐藏功能	
	if ustrregexm(`"`class'"', "book") {
	     dis in w  _col(6)  /// 
		  `"{browse "https://quqi.com/s/880197/hmpmu2ylAcvHnXwY": [计量 Books] }"' 
	}
	if ustrregexm(`"`class'"', "sj") | ustrregexm(`"`class'"', "SJ") {
	     dis _n in w  _col(6)  /// 
		  `"{browse "https://www.lianxh.cn/news/12ffe67d8d8fb.html": [Stata Journals] }"' 
		 exit
	}	
	
	
	*-help 中提到的功能
	*- 用户无输入时，只显示欢迎及资源导航页面
	if "`class'" == "" {  
       lianxh_links                    // sub-program
    }
	
	dis _n _c  // 打印一个空行
	
	*- 用户有输入情况，先根据关键词进行检索，不考虑options
	else if ustrregexm(`"`class'"', "all") {
		qui duplicates drop cat, force
		sort cat123
		local n = _N
		dis _col(20) `"{browse "https://www.lianxh.cn/news/d4d5cd7220bc7.html": -查看所有- }"' _n
/*
		local rowNum = 1
		local itemWide = 15
*/
		forvalues i =1/`n'{
		    if mod(`i',5){     // 每行呈现 # 个
			   local continue "_c"
			   local Lbar "|"
			}
			else{
			   local continue ""
			   local Lbar ""
			}
			dis website2[`i'] in y " `Lbar'" `continue'
		}
	}	
	else {
	    // 加号情况，取交集
		if ustrregexm(`"`class'"', "\+") {
		    local times         = 1
			local class_c       = "`class'"
		    while ustrregexm(`"`class_c'"', "\+"){
				local times     = `times' +1
				local class_c   =  ustrregexrf(`"`class_c'"',"\+"," ")
				local class_min =  ustrregexrf(`"`class_c'"',"\+","")
			}
			tokenize `class_c'
			forvalues i = 1/`times'{
			cap gen index_`i' = ustrregexm(title, `"``i''"')
			cap keep if index_`i' ==1
			}
			sort cat123 id
			local n = _N
			if `n' > 0 dis "连享会推文专题:" website2[1] 
			forvalues j = 1/`n' {
				if (`j'>1) & (website2[`j'] != website2[`j'-1]) {
				dis "连享会推文专题:" website2[`j']
				}
				dis "	"website[`j']				
			}
			cap save "`outcome'", replace
		}	
		else{
		    // 空格情况，取并集
			gen code = 0
			foreach name in `class'{
				cap gen index_`name' = ustrregexm(title,`"`name'"')
				cap replace code = code + index_`name'
			}
			cap keep if code != 0
			sort cat123 id
			local n = _N
			if `n' > 0 dis " 专题 >>" website2[1] 
			forvalues j = 1/`n' {
				if (`j'>1) & (website2[`j'] != website2[`j'-1]) {
				dis _n " 专题 >>" website2[`j'] 
				}
				dis "	"website[`j']				
			}
			cap save "`outcome'", replace
		} 
	
		use "`outcome'",clear
		local n = _N

	*------------------------------------------------------------------------*
	***options识别***
	/*
	  - NOTime	不输出按日期分类的推文列表
	  - Saving
	  - path() 存储路径，默认为当前工作路径
	  - Mlink
	  - Mtext
	*/ 
		
		if `n' > 0{
		
			*-首先根据是否按照时间排序的大分类进行划分
			if "`notime'" == "0" {
				capture{
					use `"`outcome'"',clear
					*global pp "/Users/kangjunjie/Documents/VScode/推文-lianxh.cn/OK-lianxh主页/主页推文列表" 
					local date = subinstr("`c(current_date)'"," ","",3)
					insobs 4, before(1)  //增加一行观察值，以便写大标题
					replace id = -9 in 1
					replace id = -8 in 2
					replace id = -7 in 3
					replace id = -6 in 4
					replace list = "## 连享会 - 推文列表 - `class'" if id==-9
					replace list = "> &emsp;     " if id==-8
					}
			}
			else {
				cap{
					keep cat123 id list linkCat cat
					format list linkCat %-20s
					sort cat123 id 
					gen id_temp = _n
					dropvars tag tag_expand list0
					egen tag = tag(cat123)
					expand 2 if tag==1, gen(tag_expand)  
					
					*-分类标题
					gen list0    = list  // 备份一下
					*replace list = "## " + linkCat if tag_expand==1   // 分类标题-含链接
					replace list = "## " + cat     if tag_expand==1   // 分类标题-仅文字
					*replace list = "  " + list if tag_expand==0
					gsort id_temp -tag -tag_exp
					*br list tag* cat* 
					
					local date = subinstr("`c(current_date)'"," ","",3)
					insobs 4, before(1)  //增加几行观察值，以便写大标题
					replace id   = -9 in 1
					replace id   = -8 in 2
					replace id   = -7 in 3
					replace id   = -6 in 4

					replace list = "## 连享会 - 推文列表" if id==-9
					replace list = "> &emsp;     " if id==-8
					replace list = "> &#x231A; Update: ``date'` &emsp;  &#x2B55; [**按时间顺序查看**](https://www.lianxh.cn/news/451e863542710.html)  " if id==-7
					replace list = "> &emsp;     " if id==-6
				}	
			}
		
			if "`saving'" ~= ""{
			    dis _n _c
				export delimited list using "`path'/`saving'" , ///
					   novar nolabel delimiter(tab) replace
				local save "`saving'"   
				*dis "md 文档快捷操作"	   
				noi dis "	"`"{stata `" view "`path'/`save'" "' : view}"' "	" `"{stata `" !open "`path'/`save'" "' : open_mac}"' "	" `"{stata `" shellout "`path'/`save'" "' : open_win}"'
				*dis "文件夹快捷操作"
				noi di "	"`"{browse `"`path'"': dir}"'
			}
		
			if "`mlink'" == "1"{
				use "`outcome'", clear
				sort cat123 id
				local n = _N
				dis " 专题:" linkCat[1]
				forvalues j = 1/`n' {
					if (`j'>1) & (website2[`j'] != website2[`j'-1]) {
					dis "专题:" linkCat[`j'] 	
					}		
					dis list[`j']
				}
				dis " "
			}
		
			if "`mtext'" == "1"{
				use "`outcome'", clear
				sort cat123 id
				local n = _N
				dis " 专题:" linkCat[1] 
				forvalues j = 1/`n' {
					if (`j'>1) & (website2[`j'] != website2[`j'-1]) {
					dis  " 专题:" linkCat[`j'] 	
					}			
					dis list2[`j']
				}
				dis " "
			}
		}
		
		else{
			dis as error `"  一无所获, 试试 {stata "   lianxh all  "} 或 {browse "https://www.lianxh.cn/blogs.html":  [推文列表]        }"' 
		}
	}
	
restore	
end

*==============================================================================*	
****Sub programs****
cap program drop lianxh_links
program define lianxh_links
version 15

	  dis    in w _col(20) _n _skip(25) "Hello, Stata!" _n
	  
	  dis in w  "Stata官方：" ///
		  `"{browse "http://www.stata.com": [Stata.com] }"' ///
		  `"{browse "http://www.stata.com/support/faqs/":   [FAQs] }"' ///
		  `"{browse "https://blog.stata.com/":      [Blogs] }"' ///
		  `"{browse "http://www.stata.com/links/resources.html":   [资源链接] }"' 
	  dis in w  _col(12)  /// 
	      `"{browse "https://www.lianxh.cn/news/aec5b661c4a82.html": [15手册] }"' ///
		  `"{browse "https://www.lianxh.cn/news/f2ad8bf464575.html": [16手册] }"' ///
		  `"{browse "https://www.lianxh.cn/news/12ffe67d8d8fb.html":   [Stata Journal] }"'  _n
		  
	  dis in w  "问问题：" ///
		  `"{browse "http://www.statalist.com": [Stata List] }"'      ///
		  `"{browse "https://stackoverflow.com":  [Stack Overflow] }"' ///
		  `"{browse "https://gitee.com/arlionn/WD":  [连享会FAQs]   }"' ///
		  `"{browse "http://bbs.pinggu.org/": [经管之家] }"'  _n
	  
	  dis in w  "资源-推文-视频：" /// 
	      `"{browse "https://www.lianxh.cn/news/d4d5cd7220bc7.html": [连享会推文] }"' ///
		  `"{browse "https://www.zhihu.com/people/arlionn/":    [知乎] }"'  ///
		  `"{browse "https://gitee.com/arlionn/Course":    [码云] }"'  ///
		  `"{browse "https://www.lianxh.cn/news/46917f1076104.html":    [计量专题] }"' ///
		  `"{browse "http://lianxh.duanshu.com":    [视频直播] }"' 
		  
	  dis in w  _col(12)  /// 
		  `"{browse "https://www.lianxh.cn/news/a630af7e186a2.html":      [Stata书单] }"' ///
		  `"{browse "https://www.lianxh.cn/news/790a2c4103539.html":  [Stata资源汇总] }"' _n
		  
	  dis in w  "在线课程：" ///
		  `"{browse "https://stats.idre.ucla.edu/stata/": [UCLA] }"' ///
		  `"{browse "http://www.princeton.edu/~otorres/Stata/":        [Princeton] }"' ///
		  `"{browse "http://wlm.userweb.mwn.de/Stata/":        [Online Stata]}"'_n
		  
	  dis in w  "学术搜索：" /// 
		  `"{browse "https://scholar.google.com/": [Google学术]   }"'  ///
		  `"{browse "http://xueshu.baidu.com/":  [百度学术]   }"' ///
		  `"{browse "https://academic.microsoft.com/home": [微软学术]   }"'  ///		  
		  `"{browse "http://scholar.chongbuluo.com/":    [学术搜索]}"'  
		  
	  dis in w  _col(11)  ///
		  `"{browse "http://scholar.cnki.net/": [CNKI]   }"' ///	  
		  `"{browse "https://sci-hub.ren":   [SCI-HUB]}"' ///
		  `"{browse "https://www.lianxh.cn/news/9e917d856a654.html":     [Links/Tools] }"' _n
		  
	  dis in w  "Data/Prog：" ///	  		  
	      `"{browse "https://www.lianxh.cn/news/e87e5976686d5.html":    [论文重现网站]   }"' ///
		  `"{browse "https://dataverse.harvard.edu/dataverse/harvard?q=stata": [Harvard dataverse]}"' ///
		  `"{browse "https://github.com/search?utf8=%E2%9C%93&q=stata&type=":      [Github]}"'  	  _n  
		  
	  dis in w _col(2) _dup(3) "~" ///
		  as smcl `"{stata "net install lianxh.pkg, from(https://lianxh.gitee.io/lianxh)": 点击更新 }"' ///
		  _dup(3) "~"
end   	



	
